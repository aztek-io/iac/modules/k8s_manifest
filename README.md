# Module: k8s_manifest

Creates an object in kubernetes.

## Example Usage

How to create a namespace using this module:

```hcl
module "namespace" {
  source   = "git@gitlab.com:aztek-io/iac/modules/k8s_manifest.git?ref=v0.1.0"
  yaml     = <<-YAML
  apiVersion: v1
  kind: Namespace
  metadata:
    name: ${var.namespace_name}
  YAML
}
```

## Argument Reference

The following arguments are supported:

### Required Attributes

* `yaml`     - (string) A yaml string.

### Optional Attributes

NONE
## Attribute Reference

NONE
