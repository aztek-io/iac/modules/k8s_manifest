resource "kubernetes_manifest" "this" {
  provider = kubernetes-alpha
  manifest = yamldecode(var.yaml)
}
